import React from "react"
import Layout from '../components/Layout'
import tw, { styled } from "twin.macro"
const Button = styled.button`
  ${tw`bg-azulino  text-white p-2 rounded`}
`
export default function Home() {
  return (
      <Layout>
        <h1>Hi people</h1>
        <Button>Activate</Button>
      </Layout>
  )
}
