module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        'azul':'#00003D',
        'rosado':'#FF007C',
        'azulino':'#00008E',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
